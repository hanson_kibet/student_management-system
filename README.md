**Student Management System**
This project contains three modules , Student module,grades module,Units module
_Student module_ performs the following functions
1. Add new Student
2. View all students
3. Search for a student by registration number
4. Update student details
5. Deleting student from the database
6. Getting the total number of students registered
_Grades module_ this module is for managing student results and performs the following functions
   1. Award marks to student
   2. view individual students grades for each unit, Total marks and Average marks
    
_Units module_ This is used to manage units to be undertaken by students
 1. Registering new units
 2. Viewing available units