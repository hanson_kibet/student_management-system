package com.studentManagement.Tests;

import com.studentManagement.Model.Student;
import com.studentManagement.Utils.DbUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.*;

class GeneralTest {
Student stud;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
    @Test
    void searchStudent() throws SQLException, ParseException, ClassNotFoundException {
        Assertions.assertEquals("828,hanson sang,Male,12-08-2021,12-12-2020,java",new Student().searchUser("828"));
    }
    @Test
    // for this test method to work replace the data with your own data
    void writeData() throws SQLException, ClassNotFoundException, ParseException {
        stud = new Student("100013","systech","Female",sdf.parse("12-12-1998"),sdf.parse("12-09-2021"),"java");
        assertEquals(true,new DbUtils().writeData("INSERT INTO students_tbl(regNo,name,gender,dob,doa,course) VALUES('"+stud.getRegNo()+"','"+stud.getName()+"','"+stud.getGender()+"','"+sdf.format(stud.getDateOfBirth())+"','"+sdf.format(stud.getDateOfAdmission())+"','"+stud.getCourse()+"')"));
    }
    @Test
    void updateStudent() throws ParseException, SQLException, ClassNotFoundException {
        stud = new Student("100013","systech","Female",sdf.parse("12-12-1998"),sdf.parse("12-09-2021"),"java");
        assertEquals(true,new DbUtils().update("update students_tbl set regNo='"+stud.getRegNo()+"',name='"+stud.getName()+"',gender='"+stud.getGender()+"',dob='"+sdf.format(stud.getDateOfBirth())+"',doa='"+sdf.format(stud.getDateOfAdmission())+"',course='"+stud.getCourse()+"' where regNo='"+100013+"'"));
    }
    @Test
    void delete() throws SQLException, ClassNotFoundException, ParseException {
        //creating new student
        stud = new Student("44444","test name","Female",sdf.parse("12-12-1998"),sdf.parse("12-09-2021"),"java");
        new DbUtils().writeData("INSERT INTO students_tbl(regNo,name,gender,dob,doa,course) VALUES('"+stud.getRegNo()+"','"+stud.getName()+"','"+stud.getGender()+"','"+sdf.format(stud.getDateOfBirth())+"','"+sdf.format(stud.getDateOfAdmission())+"','"+stud.getCourse()+"')");
        // testing delete() method
        assertEquals(true,new DbUtils().delete("DELETE FROM students_tbl WHERE regNo='"+stud.getRegNo()+"'"));
    }

}