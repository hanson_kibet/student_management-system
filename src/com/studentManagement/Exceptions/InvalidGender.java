package com.studentManagement.Exceptions;

public class InvalidGender extends Exception{
    public InvalidGender() {
        super("Invalid gender either Male or Female is accepted only");
    }
}
