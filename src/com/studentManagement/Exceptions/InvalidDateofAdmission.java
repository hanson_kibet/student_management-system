package com.studentManagement.Exceptions;

public class InvalidDateofAdmission extends Exception{
    public InvalidDateofAdmission() {
        super("Invalid date of admission format!! please enter in format dd-mm-yyyy");
    }
}
