package com.studentManagement.Interfaces;

import com.studentManagement.Model.Units;

import java.sql.SQLException;
import java.util.ArrayList;

public interface UnitsI {
    public boolean addUnit() throws SQLException, ClassNotFoundException;
    public void deleteUnit() throws SQLException, ClassNotFoundException;
    public ArrayList<Units> displayUnits() throws SQLException, ClassNotFoundException;
    public void updateUnits() throws SQLException, ClassNotFoundException;
}
