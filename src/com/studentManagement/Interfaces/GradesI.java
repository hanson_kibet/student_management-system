package com.studentManagement.Interfaces;

import com.studentManagement.Model.Grades;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

public interface GradesI {
    void add() throws SQLException, ClassNotFoundException, ParseException; //add Grades
    ArrayList<Grades> displayGrades(String regNo) throws SQLException, ClassNotFoundException;//view Grades
}
