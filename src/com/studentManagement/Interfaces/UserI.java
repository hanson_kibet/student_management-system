package com.studentManagement.Interfaces;

import com.studentManagement.Exceptions.InvalidDateofBirth;
import com.studentManagement.Model.Student;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public interface UserI {

    void addUser() throws SQLException, ParseException, ClassNotFoundException, InvalidDateofBirth;

    void viewUser() throws SQLException, ClassNotFoundException, ParseException;

    void deleteUser(String sql, String regNo) throws SQLException, ParseException, ClassNotFoundException;

    String searchUser(String regNo) throws SQLException, ParseException, ClassNotFoundException;

    void updateUser(String regNo, Scanner sc) throws SQLException, ParseException, ClassNotFoundException;

    int compareTo(Student student);
}
