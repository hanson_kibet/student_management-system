package com.studentManagement.Utils;

import java.sql.*;

public class DbUtils {
    final String className = "com.mysql.jdbc.Driver";
    private final  Connection conn;
    private Statement stm;
    private PreparedStatement pst;
    final String username = "root";
    final String url = "jdbc:mysql://localhost:3306/schoolDb";
    final String password = "";
    public DbUtils() throws ClassNotFoundException, SQLException {
        Class.forName(className);
        conn= DriverManager.getConnection(url, username, password);
    }

    @Override
    protected void finalize() throws Throwable {
        this.closeConnections();
    }

    public ResultSet readData(String sql) throws SQLException {
        stm = conn.createStatement();
        return stm.executeQuery(sql);
    }
    public boolean writeData(String sql) throws SQLException {
        stm = conn.createStatement();
        return (stm.executeUpdate(sql)==0)?false:true;
    }
    public boolean update(String sql) throws SQLException {
        stm = conn.createStatement();
        return (stm.executeUpdate(sql) == 0)?false:true;
    }
    public boolean delete(String sql) throws SQLException {
        stm = conn.createStatement();
        return (stm.executeUpdate(sql) == 0)?false:true;
    }
    public int getCount(String sql) throws SQLException { // for retrieving the number of record in the database table
        stm = conn.createStatement();
        ResultSet rs = stm.executeQuery(sql);
        rs.next();
        return rs.getInt(1);
    }
    private void closeConnections() throws SQLException {
        stm.close();
        conn.close();
    }
}
