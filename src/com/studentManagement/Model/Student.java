package com.studentManagement.Model;
import com.studentManagement.Enum.Gender;
import com.studentManagement.Exceptions.InvalidDateofAdmission;
import com.studentManagement.Exceptions.InvalidDateofBirth;
import com.studentManagement.Interfaces.UserI;
import com.studentManagement.Utils.DbUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Student implements UserI {
    private String regNo;
    private String name;
    private Gender gender;
    private Date dateOfBirth;
    private Date dateOfAdmission;
    private String course;
    DbUtils db;
    static Scanner sc;
    public Student(String regNo, String name, String genders, Date dateOfBirth, Date dateOfAdmission, String course) {
        this.regNo = regNo;
        this.name = name;
        this.gender = Gender.valueOf(genders);
        this.dateOfBirth = dateOfBirth;
        this.dateOfAdmission = dateOfAdmission;
        this.course = course;
    }

    public Student() {

    }
    @Override
    public void addUser() throws SQLException, ClassNotFoundException{
        this.addStudent();
    }

    @Override
    public void viewUser() throws SQLException, ClassNotFoundException, ParseException {
        for (Student s : this.viewStudents())
            System.out.println(s);
    }

    @Override
    public void deleteUser(String sql, String regNo) throws SQLException, ParseException, ClassNotFoundException {
        this.deleteStudent(sql,regNo);
    }

    @Override
    public String searchUser(String regNo) throws SQLException, ParseException, ClassNotFoundException {
        return this.searchStudent(regNo);
    }

    @Override
    public void updateUser(String regNo, Scanner sc) throws SQLException, ParseException, ClassNotFoundException {
        this.updateStudent(regNo,sc);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Student other = (Student) obj;
        if (regNo == null){
            if (other.regNo != null)
                return false;
        }
        else if (!regNo.equals(other.regNo))
            return false;
        return true;
    }
    @Override
    public int compareTo(Student student) {
        return regNo.compareTo(student.regNo);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((regNo == null) ? 0 : (regNo.hashCode()));
        return result;
    }


    public boolean addStudent() throws SQLException, ClassNotFoundException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
        Student stud = null;
        boolean result;
        try {
            stud = getStudentData();
        } catch (InvalidDateofAdmission invalidDateofAdmission) {
            System.out.println(invalidDateofAdmission.getMessage());
        }catch (InvalidDateofBirth e){
            System.out.println(e.getMessage());
        }
        db = new DbUtils();
            result = db.writeData("INSERT INTO students_tbl(regNo,name,gender,dob,doa,course) VALUES('"+stud.getRegNo()+"','"+stud.getName()+"','"+stud.getGender()+"','"+sdf.format(stud.getDateOfBirth())+"','"+sdf.format(stud.getDateOfAdmission())+"','"+stud.getCourse()+"')");
            System.out.println("Student data added successfully");
    return result;
    }
    private void deleteStudent(String sql, String regNo1) throws SQLException, ParseException, ClassNotFoundException {
        db= new DbUtils();
        Scanner scanner = new Scanner(System.in);
        boolean available = false;
        for (Student j : this.viewStudents()) {
            if (j.getRegNo().equals(regNo1)){
                System.out.println("The following student's records will be deleted\n");
                System.out.println(j);
                available = true;
                System.out.println("Are you sure you want to delete ? enter y / n \n");
                String option = scanner.nextLine();
                if (option.equalsIgnoreCase("y")){
                    db.delete(sql);
                    System.out.println("Student records deleted successfully\n");
                }
                else {
                    break;
                }
                break;
            }
        }
        if (available != true){

            System.out.println("No student with such registration number found");
        }
    }
    public ArrayList<Student> viewStudents() throws SQLException, ClassNotFoundException, ParseException {
        ArrayList<Student> sl = new ArrayList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        db=new DbUtils();
        ResultSet rs =db.readData("select * from students_tbl");
        while (rs.next()){
            sl.add(new Student(rs.getString("regNo"),rs.getString("name"),rs.getString("gender"),simpleDateFormat.parse(rs.getString("dob")),simpleDateFormat.parse(rs.getString("doa")),rs.getString("course")));
        }
        return sl;
    }
    private String searchStudent(String regNo) throws SQLException, ParseException, ClassNotFoundException {
        String result = "";
        boolean found = false;
        for (Student j : this.viewStudents()) {
            if (j.getRegNo().equals(regNo)){
                result = String.valueOf(j);
                found = true;
                break;
            }
        }
        if (found != true)
        {
            result = "No student with such registration number found";
        }
        return result;
    }
    public Student checkStudent(String regNumber) throws SQLException, ParseException, ClassNotFoundException {
        Student result = null;
        boolean found = false;
        for (Student j : this.viewStudents()) {
            if (j.getRegNo().equals(regNumber)){
                result = j;
                found = true;
                break;
            }
        }
        if (found != true)
        {
            System.out.println("No student with such registration number found");
        }
        return result;
    }
    private void updateStudent(String regNo2,Scanner sc) throws SQLException, ParseException, ClassNotFoundException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
        boolean found1 = false;
        db = new DbUtils();
        for (Student j : this.viewStudents()) {
            if (j.getRegNo().equals(regNo2)){
                System.out.println("The following student's records will be updated");
                System.out.println(j);
                found1 = true;
                System.out.println("Are you sure you want to continue ? enter y / n");
                String option = sc.nextLine();
                if (option.equalsIgnoreCase("y")){
                    try {
                        Student stud = getStudentData();
                        db.update("update students_tbl set regNo='"+stud.getRegNo()+"',name='"+stud.getName()+"',gender='"+stud.getGender()+"',dob='"+sdf.format(stud.getDateOfBirth())+"',doa='"+sdf.format(stud.getDateOfAdmission())+"',course='"+stud.getCourse()+"' where regNo='"+regNo2+"'");
                        System.out.println("Student records updated successfully");
                    }catch (Exception e){
                        System.out.println("Failed to update student data due to:\t"+e);
                    }

                }
                else {
                    break;
                }
                break;
            }
        }
        if (found1 != true){
            System.out.println("No such students records found");
        }
    }
    public String getRegNo() {
        return regNo;
    }

    public String getName() {
        return name;
    }

    public Gender getGender() {
        return gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public Date getDateOfAdmission() {
        return dateOfAdmission;
    }

    public String getCourse() {
        return course;
    }

    private static Student getStudentData() throws InvalidDateofBirth, InvalidDateofAdmission {
        Date dob, doa;
        String regNo,name,dateOfBirth,dateOfAdmission,gender,course;
        sc = new Scanner(System.in);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        System.out.println("Enter registration number:\n");
        regNo = sc.nextLine();
        System.out.println("Enter student name:\n");
        name = sc.nextLine();
        System.out.println("Enter gender (Male or Female):\n");
        gender = sc.nextLine();
        System.out.println("Enter date of birth (dd-mm-yyyy):\n");
        dateOfBirth = sc.nextLine();
        try {
            dob = simpleDateFormat.parse(dateOfBirth);
        }
        catch (ParseException e){
            throw new InvalidDateofAdmission();
        }
        System.out.println("Enter date of admission (dd-mm-yyyy):\n");
        dateOfAdmission = sc.nextLine();
        try{
            doa = simpleDateFormat.parse(dateOfAdmission);
        }catch (ParseException e){
            throw new InvalidDateofBirth();
        }
        System.out.println("Enter course:\n");
        course = sc.nextLine();
        Student stud = new Student(regNo,name,toCamelCase(gender),dob,doa,course);
        return stud;
    }

    private static String toCamelCase(String genderCase) {
        return genderCase.substring(0,1).toUpperCase()+genderCase.substring(1);
    }

    @Override
    public String toString() {
        return "" +regNo+
                "," +getName()+
                "," +getGender()+
                "," +new SimpleDateFormat("dd-MM-YYYY").format(getDateOfBirth())+
                "," +new SimpleDateFormat("dd-MM-YYYY").format(getDateOfAdmission())+
                "," +getCourse();
    }
}
