package com.studentManagement.Model;

import com.studentManagement.Interfaces.UnitsI;
import com.studentManagement.Utils.DbUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class Units implements UnitsI {
    public String unitCode;
    public String unitTitle;
    private DbUtils db;
    Scanner sc = new Scanner(System.in);

    public Units(String unitCode, String unitTitle) {
        this.unitCode = unitCode;
        this.unitTitle = unitTitle;
    }

    public Units() {

    }

    public String getUnitCode() {
        return unitCode;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode;
    }

    public String getUnitTitle() {
        return unitTitle;
    }

    public void setUnitTitle(String unitTitle) {
        this.unitTitle = unitTitle;
    }

    @Override
    public boolean addUnit() throws SQLException, ClassNotFoundException {
        db = new DbUtils();
        System.out.println("Enter Unit code");
        String unitCode=sc.nextLine();
        System.out.println("Enter Unit title");
        String unitTitle = sc.nextLine();
        return db.writeData("INSERT INTO subjects_tbl(code,title) VALUES('"+unitCode+"','"+unitTitle+"')");
    }

    @Override
    public void deleteUnit() throws SQLException, ClassNotFoundException {
        System.out.println("choose the unit to delete from the list below\n");
        for (Units u : this.displayUnits())
            System.out.println(u);
        System.out.println("Enter unit code to delete");
        String unitCode = sc.nextLine();
        boolean available = false;
        for (Units j : this.displayUnits()) {
            if (j.getUnitCode().equals(unitCode)){
                System.out.println("The following Unit's records will be deleted\n");
                System.out.println(j);
                available = true;
                System.out.println("Are you sure you want to delete ? enter y / n \n");
                String option = sc.nextLine();
                if (option.equalsIgnoreCase("y")){
                    db.delete("DELETE FROM subjects_tbl WHERE code='"+j.getUnitCode()+"'");
                    System.out.println("Unit records deleted successfully\n");
                }
                else {
                    break;
                }
                break;
            }
        }
        if (available != true){

            System.out.println("No Unit with such unit code found");
        }
    }

    @Override
    public ArrayList<Units> displayUnits() throws SQLException, ClassNotFoundException {
        ArrayList<Units> sl = new ArrayList<>();
        db=new DbUtils();
        ResultSet rs =db.readData("select * from subjects_tbl");
        while (rs.next()){
            sl.add(new Units(rs.getString("code"),rs.getString("title")));
        }
        return sl;
    }

    @Override
    public void updateUnits() throws SQLException, ClassNotFoundException {
        db = new DbUtils();
        System.out.println("choose the unit to update from the list below\n");
        for (Units u : this.displayUnits())
            System.out.println(u);
        System.out.println("Enter unit code to update");
        String unitCode = sc.nextLine();
        boolean available = false;
        for (Units j : this.displayUnits()) {
            if (j.getUnitCode().equals(unitCode)){
                System.out.println("The following Unit's records will be deleted\n");
                System.out.println(j);
                available = true;
                System.out.println("Are you sure you want to delete ? enter y / n \n");
                String option = sc.nextLine();
                if (option.equalsIgnoreCase("y")){
                    System.out.println("Enter new Unit code");
                    String unitCode1=sc.nextLine();
                    System.out.println("Enter new Unit title");
                    String unitTitle = sc.nextLine();
                    db.update("update subjects_tbl set code='"+unitCode1+"',title='"+unitTitle+"' where code='"+j.getUnitCode()+"'");
                    System.out.println("Unit records updated successfully\n");
                }
                else {
                    break;
                }
                break;
            }
        }
        if (available != true){

            System.out.println("No Unit with such unit code found");
        }
    }

    @Override
    public String toString() {
        return "" +getUnitCode()+
                " " +getUnitTitle();
    }
}
