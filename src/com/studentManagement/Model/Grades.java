package com.studentManagement.Model;

import com.studentManagement.Interfaces.GradesI;
import com.studentManagement.Utils.DbUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Grades extends Units implements GradesI {
    public String regNo;
    public String name;
    public String Unit;
    public String User;
    public double score;
    DbUtils db;

    public Grades(String regNo, String name, String unit, String user, double score) {
        this.regNo = regNo;
        this.name = name;
        Unit = unit;
        User = user;
        this.score = score;
    }

    public Grades() {

    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public String getUser() {
        return User;
    }

    public void setUser(String user) {
        User = user;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
Student student = new Student();
    @Override
    public void add() throws SQLException, ClassNotFoundException, ParseException {
        db= new DbUtils();
        System.out.println("Enter registration number of student to award marks");
        Scanner scanner = new Scanner(System.in);
        String regNo= scanner.nextLine();
        boolean available = false;
        for (Student j : student.viewStudents()) {
            if (j.getRegNo().equals(regNo)){
                System.out.println("The student with the following details found\n");
                System.out.println(j);
                available = true;
                System.out.println("Are you sure you want to give marks to the student selected ? enter y / n \n");
                String option = scanner.nextLine();
                if (option.equalsIgnoreCase("y")){
                    //choosing the unit
                    System.out.println("The following are the available units");
                    Units u = new Units();
                    for (Units units : u.displayUnits()){
                        System.out.println(units);
                    }
                    System.out.println("Enter unit code from the units above");
                    String code = scanner.nextLine();
                    addScore(code,j.getRegNo(),j.getName());
                }
                else {
                    break;
                }
                break;
            }
        }
        if (available != true){

            System.out.println("No student with such registration number found");
        }
    }

    private void addScore(String code, String regNo, String name) throws SQLException, ClassNotFoundException {
        Scanner scanner = new Scanner(System.in);
        boolean available = false;
        for (Units j : this.displayUnits()) {
            if (j.getUnitCode().equals(code)){
                System.out.println("You are about to award marks for the unit below\n");
                System.out.println(j);
                available = true;
                System.out.println("Are you sure you want to give marks? enter y / n \n");
                String option = sc.nextLine();
                if (option.equalsIgnoreCase("y")){
                    System.out.println("Enter Score");
                    Double score = scanner.nextDouble();
                    scanner.nextLine();
                    System.out.println("Enter your name");
                    String username = scanner.nextLine();
                    db.writeData("INSERT INTO grades_tbl(regNo,name,unit,score,addedBy) VALUES('"+regNo+"','"+name+"','"+j.getUnitTitle()+"','"+score+"','"+username+"')");
                    System.out.println("Student marks added successfully\n");
                }
                else {
                    break;
                }
                break;
            }
        }
        if (available != true){

            System.out.println("No Unit with such unit code found");
        }
    }
    @Override
    public ArrayList<Grades> displayGrades(String regNo) throws SQLException, ClassNotFoundException {
        db = new DbUtils();
        ArrayList<Grades> sl = new ArrayList<>();
        ResultSet rs =db.readData("select * from grades_tbl WHERE regNo='"+regNo+"'");
        while (rs.next()){
            sl.add(new Grades(rs.getString("regNo"),rs.getString("name"),rs.getString("unit"),rs.getString("addedBy"),rs.getDouble("score")));
        }
        return sl;
    }

    @Override
    public String toString() {
        return "" +getRegNo()+
                ", " +getName()+
                ", " +getUnit()+
                ", " +getScore()+"%"+
                ",Graded by\t:" +getUser();
    }
}
