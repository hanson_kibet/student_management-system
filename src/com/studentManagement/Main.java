package com.studentManagement;
import com.studentManagement.Model.Grades;
import com.studentManagement.Model.Student;
import com.studentManagement.Model.Units;
import com.studentManagement.Utils.DbUtils;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException, SQLException, ClassNotFoundException {
        Scanner sc = new Scanner(System.in);
        Student student = new Student();
        int choice;
        String regNo;
        System.out.println("Welcome to Student Management System\n");
        do {
            System.out.println("\n1. student module\n" +
                    "2. students Grades\n" +
                    "3. units\n" +
                    "0. Exit");
        choice = sc.nextInt();
        sc.nextLine();
        switch (choice){
            case 1:
                studentMenu();
                break;
            case 2:
                viewStudentGrades();
                break;
            case 3:
                viewUnits();
                break;

        }
        }
        while(choice != 0);
    }

    private static void studentMenu() throws SQLException, ClassNotFoundException, ParseException {
        Scanner sc = new Scanner(System.in);
        Student student = new Student();
        int choice;
        String regNo;
        System.out.println("Welcome to Student Management System\n");
        do {
            System.out.println("\n1. Add new student\n" +
                    "2. view students\n" +
                    "3. search students\n" +
                    "4. delete students\n" +
                    "5. update students\n" +
                    "6. Total number of students\n"+
                    "0. Exit");
            choice = sc.nextInt();
            sc.nextLine();
            switch (choice){
                case 1:
                    student.addUser();
                    break;
                case 2:
                    student.viewUser();
                    break;
                case 3:
                    System.out.println("Please enter registration number to search");
                    regNo = sc.nextLine();
                    System.out.println(student.searchUser(regNo));
                    break;
                case 4:
                    System.out.println("Please enter registration number for student you want to delete\n");
                    regNo = sc.nextLine();
                    student.deleteUser("DELETE FROM students_tbl WHERE regNo='"+regNo+"'",regNo);
                    break;
                case 5:
                    System.out.println("Please enter registration number for student you want to update");
                    regNo = sc.nextLine();
                    student.updateUser(regNo,sc);
                    break;
                case 6:
                    System.out.println("Total = "+new DbUtils().getCount("select count(*) from students_tbl")+"\tStudents");
                    break;

            }
        }
        while(choice != 0);
    }

    private static void viewUnits() throws SQLException, ClassNotFoundException {
        Units units = new Units();
        Scanner scanner = new Scanner(System.in);
        int option;
        do {
            System.out.println("Welcome to student Units module\n" +
                    "1. add new Units\n" +
                    "2. view Available units\n" +
                    "3. delete units\n" +
                    "4. update units\n"+
                    "0. exit module\n");
            option = scanner.nextInt();
            scanner.nextLine();
            switch (option){
                case 1:
                    String s = (units.addUnit()) ? "Units registered successfully" : "Units registration failed";
                    System.out.println(s);
                    break;
                case 2:
                    for (Units u : units.displayUnits())
                        System.out.println(u);
                    break;
                case 3:
                    units.deleteUnit();
                    break;
                case 4:
                    units.updateUnits();
                    break;
            }

        }while (option != 0);
    }

    private static void viewStudentGrades() throws SQLException, ParseException, ClassNotFoundException {
        Grades gd = new Grades();
        Scanner scanner = new Scanner(System.in);
        int option;
        do {
            System.out.println("Welcome to student grades module\n" +
                    "1. add grades\n" +
                    "2. view student grades\n" +
                    "0. exit module\n");
            option = scanner.nextInt();
            scanner.nextLine();
            switch (option){
                case 1:
                    gd.add();
                    break;
                case 2:
                    viewGrades(gd);
                    break;
            }

        }while (option != 0);
    }

    private static void viewGrades(Grades gd) throws SQLException, ClassNotFoundException, ParseException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter registration number to check grades");
        String regNo = sc.nextLine();
        Student std = new Student().checkStudent(regNo);
        double sum = 0.0;
        if (std != null){
            System.out.println("results for "+std.getRegNo()+" "+std.getName());
            for (Grades g : gd.displayGrades(regNo)) {
                System.out.println(g.getUnit() + " " + g.getScore() + " Added by" + g.getUser());
                sum += g.getScore();
            }
            System.out.println("Total marks :"+sum);
            double average = sum/gd.displayGrades(regNo).size();
            System.out.println("Average = "+average);
        }
    }

}
